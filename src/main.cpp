#include <Arduino.h> // not needed for Arduino IDE
#include <MD_MAX72xx.h>
#include <MD_Parola.h>
#include <RTClib.h>
#include <SPI.h>
#include <Wire.h>

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CLK_PIN 10
#define DATA_PIN 12
#define CS_PIN 11

RTC_DS1307 rtc;
MD_Parola matrix = MD_Parola(HARDWARE_TYPE, DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);

char displayBuffer[100];

const char *weekdays[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

void setup() {
  Serial.begin(115200);
  Serial.println("Starting...");

  matrix.begin();
  Wire.begin();
  rtc.begin();

  matrix.setIntensity(0);

  if (rtc.isrunning()) {
    DateTime initDateTime = DateTime(__DATE__, __TIME__);
    if (rtc.now() < initDateTime) rtc.adjust(initDateTime);
  } else {
    Serial.println("RTC is NOT running!");
  }
}

void loop() {
  matrix.setTextAlignment(PA_CENTER);

  // // DATE
  // DateTime now        = rtc.now();
  // const char *weekday = weekdays[now.dayOfTheWeek()];
  // sprintf(displayBuffer, "%s %d/%02d/%d", weekday, now.month(), now.day(), now.year());
  // matrix.print(displayBuffer);
  // delay(10000);

  // // TIME
  // for (int i = 0; i < 10; i++)
  // {
  //     now              = rtc.now();
  //     const char *ampm = now.hour() > 12 ? "PM" : "AM";
  //     int hour         = now.hour() > 12 ? now.hour() - 12 : now.hour();
  //     sprintf(displayBuffer, "%d:%02d:%02d %s", hour, now.minute(), now.second(), ampm);
  //     matrix.print(displayBuffer);
  //     delay(1000);
  // }

  // // TEMP
  // double temperature = 72.5; // get this from sensor
  // char temperatureBuffer[6]; // create buffer to format a double/float value as a string
  // dtostrf(temperature, 2, 1, temperatureBuffer);  // format the double/float as a string with 2 minimum width and 1 decimal precision
  // sprintf(displayBuffer,"%s*F", temperatureBuffer); // format the new temperature string by adding *F at the end
  // matrix.print(displayBuffer);
  // delay(10000);

  DateTime now = rtc.now();
  const char *ampm = now.hour() > 12 ? "p" : "a";
  int hour = now.hour() > 12 ? now.hour() - 12 : (now.hour() == 0 ? 12 : now.hour());
  sprintf(displayBuffer, "%d:%02d%s", hour, now.minute(), ampm);
  matrix.print(displayBuffer);
  delay(1000);
}
